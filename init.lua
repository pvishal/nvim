-- Set <space> as the leader key
-- See `:help mapleader`
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Recommended by nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- [[ Setting options ]]
require("config.set")

-- [[ Package manager and packages ]]
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

-- NOTE: Here is where you install your plugins.
--  You can configure plugins using the `config` key.
--
--  You can also configure plugins after the setup call,
--    as they will be available in your neovim runtime.
require('lazy').setup({
    -- NOTE: First, some plugins that don't require any configuration

    -- Detect tabstop and shiftwidth automatically
    {
      'tpope/vim-sleuth',
      commit = "1cc4557420f215d02c4d2645a748a816c220e99b",
    },

    -- Git related plugins
    {
      'tpope/vim-fugitive',
      commit = "5f0d280b517cacb16f59316659966c7ca5e2bea2",
    },

    {
      -- Pretty diff viewer
      'sindrets/diffview.nvim',
      commit = "8c1702470fd5186cb401b21f9bf8bdfad6d5cc87",
      requires = 'nvim-lua/plenary.nvim',
      config = function()
        require("diffview").setup {
          view = {
            merge_tool = {
              -- Config for conflicted files in diff views during a merge or rebase.
              layout = "diff3_mixed",
              -- Temporarily disable diagnostics for conflict buffers while in the view.
              disable_diagnostics = true,
              winbar_info = true,
            },
          },
        }
      end,
    },

    {
      'TimUntersberger/neogit',
      commit = "efc1764060e64cc8e81687319d5223b3246e5871",
      config = function()
        require("neogit").setup {
          auto_refresh = false,
          integrations = {
            diffview = true,
          },
        }
      end,
    },

    {
      "rbong/vim-flog",
      commit = "21dd4d655339560d0f4b876852ef0cc8ff16b7f1",
      cmd = { "Flog", "Flogsplit", "Floggit" },
      dependencies = { "vim-fugitive" },
    },

    -- NOTE: This is where your plugins related to LSP can be installed.
    --  The configuration is done below. Search for lspconfig to find it below.
    {
      -- LSP Configuration & Plugins
      'neovim/nvim-lspconfig',
      commit = "5f7a8311dd6e67de74c12fa9ac2f1aa75f72b19e",
      dependencies = {
        -- Automatically install LSPs to stdpath for neovim
        {
          'williamboman/mason.nvim',
          commit = "057ac5ca159c83e302a55bd839a96ff1ea2396db",
          config = true
        },
        {
          'williamboman/mason-lspconfig.nvim',
          commit = "43f2ddf0b5f544d9bb20e24b8ff76b79f2a07ab0",
        },

        -- Useful status updates for LSP
        -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
        {
          'j-hui/fidget.nvim',
          commit = "0ba1e16d07627532b6cae915cc992ecac249fb97",
          opts = {}
        },

        -- Additional lua configuration, makes nvim stuff amazing!
        {
          'folke/neodev.nvim',
          commit = "7e3f718f43de41053f77b1e8de6de2b759fc4023",
        },
      },
    },

    {
      -- Autocompletion
      'hrsh7th/nvim-cmp',
      dependencies = { 'hrsh7th/cmp-nvim-lsp', 'L3MON4D3/LuaSnip', 'saadparwaiz1/cmp_luasnip' },
    },

    -- Useful plugin to show you pending keybinds.
    { 'folke/which-key.nvim',  opts = {} },
    {
      -- Adds git releated signs to the gutter, as well as utilities for managing changes
      'lewis6991/gitsigns.nvim',
      opts = {
        -- See `:help gitsigns.txt`
        signs = {
          add = { text = '+' },
          change = { text = '~' },
          delete = { text = '_' },
          topdelete = { text = '‾' },
          changedelete = { text = '~' },
        },
      },
    },

    -- Lazy
    {
      "navarasu/onedark.nvim",
      lazy = false,
      priority = 1000, -- Ensure it loads first
      config = function()
        require('onedark').setup {
          style = 'darker',
        }
        vim.cmd("colorscheme onedark")
      end,
    },

    {
      -- Set lualine as statusline
      'nvim-lualine/lualine.nvim',
      -- See `:help lualine.txt`
      opts = {
        options = {
          icons_enabled = false,
          theme = 'onedark',
          component_separators = '|',
          section_separators = '',
        },
        globalstatus = true,
        winbar = {
          lualine_a = {},
          lualine_b = {},
          lualine_c = {},
          lualine_x = {
            {
              'filename',
              file_status = true,
              path = 1,
            },
          },
          lualine_y = {},
          lualine_z = {}
        },

        inactive_winbar = {
          lualine_a = {},
          lualine_b = {},
          lualine_c = {},
          lualine_x = {
            {
              'filename',
              file_status = true,
              path = 1,
            },
          },
          lualine_y = {},
          lualine_z = {}
        },
      },
    },

    {
      -- Automatically complete brackets and quotes
      "windwp/nvim-autopairs",
      name = "nvim-autopairs",
      opt = true,
      event = "InsertEnter",
      module = { "nvim-autopairs.completion.cmp", "nvim-autopairs" },
      config = function()
        require("nvim-autopairs").setup {
          enable_check_bracket_line = true,
        }
      end,
    },

    {
      -- Add indentation guides even on blank lines
      'lukas-reineke/indent-blankline.nvim',
      -- Enable `lukas-reineke/indent-blankline.nvim`
      -- See `:help indent_blankline.txt`
      opts = {
        char = '┊',
        show_trailing_blankline_indent = false,
      },
    },

    -- "gc" to comment visual regions/lines
    { 'numToStr/Comment.nvim', opts = {} },

    -- Fuzzy Finder (files, lsp, etc)
    {
      'nvim-telescope/telescope.nvim',
      commit = "c1a2af0af69e80e14e6b226d3957a064cd080805",
      dependencies = {
        'nvim-lua/plenary.nvim',
        commit = "9ac3e9541bbabd9d73663d757e4fe48a675bb054",
      }
    },

    -- Fuzzy Finder Algorithm which requires local dependencies to be built.
    -- Only load if `make` is available. Make sure you have the system
    -- requirements installed.
    {
      'nvim-telescope/telescope-fzf-native.nvim',
      commit = "580b6c48651cabb63455e97d7e131ed557b8c7e2",
      -- NOTE: If you are having trouble with this installation,
      --       refer to the README for telescope-fzf-native for more instructions.
      build = 'make',
      cond = function()
        return vim.fn.executable 'make' == 1
      end,
    },

    {
      -- Highlight, edit, and navigate code
      'nvim-treesitter/nvim-treesitter',
      commit = "b4d2640eab4b1f6373e1ded84ab9f6db0c02c756",
      dependencies = {
        {
          'nvim-treesitter/nvim-treesitter-textobjects',
          commit = "85a6f9d1af8b731f09f4031e1f9478d4b40d13ab",
        },
      },
      build = ":TSUpdate",
    },

    {
      "nvim-tree/nvim-web-devicons",
      name = "nvim-web-devicons",
      config = function()
        require("nvim-web-devicons").setup {
          default = true,
        }
      end,
    },

    {
      -- nvim-tree
      "nvim-tree/nvim-tree.lua",
      opt = true,
      config = function()
        require("nvim-tree").setup {
          disable_netrw = false,
          hijack_netrw = true,
          respect_buf_cwd = true,
          view = {
            width = 40,
            number = true,
            relativenumber = true,
          },
          filters = {
            custom = { ".git" },
          },
          actions = {
            open_file = {
              quit_on_open = true,
              window_picker = {
                enable = true,
              },
            },
          },
          sync_root_with_cwd = true,
          update_focused_file = {
            enable = true,
            update_root = true,
          },
        }
      end,
    },

    { -- To help close buffers nicely
      "moll/vim-bbye",
    },

    {
      -- Buffer line
      "akinsho/nvim-bufferline.lua",
      event = "BufReadPre",
      config = function()
        require("bufferline").setup {
          options = {
            numbers = "none",
            mode = "buffers",
            -- Use the close command from vim-bbye
            close_command = "Bdelete! %d",
            diagnostics = "nvim_lsp",
            separator_style = "thick",
            show_tab_indicators = true,
            show_buffer_close_icons = false,
            show_close_icon = false,
            color_icons = true,
            -- Don't draw the bufferline over the nvim-tree sidebar
            offsets = { { filetype = "NvimTree", text = "", padding = 0 } },
          }
        }
      end,
    },

    {
      -- Keep the function context on the top
      "nvim-treesitter/nvim-treesitter-context",
      config = function()
        require("treesitter-context").setup {
          enable = true,
        }
      end,
    },

    {
      -- Highlight word under the cursor
      "RRethy/vim-illuminate",
      config = function()
        require("illuminate").configure {
          delay = 200,
          filetype_overrides = {},
          filetypes_denylist = {
            'dirvish',
            'fugitive',
          },
          filetypes_allowlist = {},
          -- Only highlight in normal mode.
          modes_allowlist = { 'n' },
          -- Highlight only if there are at least two instances to show.
          min_count_to_highlight = 2,
        }
      end,
    },

    { -- Github copilot
      "github/copilot.vim",
    },

    -- TODO(Vishal): Refactor this into LSP plugin config
    require "user.plugins.autoformat",
  },
  {
    defaults = { lazy = false, version = "57cce98dfdb2f2dd05a0567d89811e6d0505e13b" },
  })

-- [[ Configure Illuminate ]]
-- TODO(Vishal): Move to keymap/autocmd files maybe?
vim.keymap.set('n', '<leader>i', function()
  require('illuminate').toggle_visibility_buf()
end)
local augroup = 'my-vim-illuminate-autocmds'
vim.api.nvim_create_augroup(augroup, { clear = true })
vim.api.nvim_create_autocmd('CursorMoved', {
  group = augroup,
  callback = function()
    require('illuminate').invisible_buf()
  end,
})


-- [[ Configure Telescope ]]
-- See `:help telescope` and `:help telescope.setup()`
require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
        ['<C-d>'] = false,
      },
    },
  },
}

-- Enable telescope fzf native, if installed
pcall(require('telescope').load_extension, 'fzf')


-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`
require('nvim-treesitter.configs').setup {
  -- Add languages to be installed here that you want installed for treesitter
  ensure_installed = { 'c', 'cpp', 'lua', 'python', 'vimdoc', 'vim' },

  -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
  auto_install = false,

  highlight = { enable = true },
  indent = { enable = true, disable = { 'python' } },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
      scope_incremental = '<c-s>',
      node_decremental = '<M-space>',
    },
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['aa'] = '@parameter.outer',
        ['ia'] = '@parameter.inner',
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
    swap = {
      enable = true,
      swap_next = {
        ['<leader>a'] = '@parameter.inner',
      },
      swap_previous = {
        ['<leader>A'] = '@parameter.inner',
      },
    },
  },
}

-- LSP settings.
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  -- NOTE: Remember that lua is a real programming language, and as such it is possible
  -- to define small helper and utility functions so you don't have to repeat yourself
  -- many times.
  --
  -- In this case, we create a function that lets us more easily define mappings specific
  -- for LSP related items. It sets the mode, buffer and description for us each time.
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
  nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

  nmap('gd', vim.lsp.buf.definition, '[G]oto [D]efinition')
  nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  nmap('gI', vim.lsp.buf.implementation, '[G]oto [I]mplementation')
  nmap('<leader>D', vim.lsp.buf.type_definition, 'Type [D]efinition')
  nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
  nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

  -- See `:help K` for why this keymap
  nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

  -- Lesser used LSP functionality
  nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  nmap('<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[W]orkspace [L]ist Folders')

  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
end

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
local servers = {
  clangd = {},
  -- pyright = {},
  -- bashls = {},
  -- cmake = {},
  lua_ls = {
    Lua = {
      workspace = { checkThirdParty = false },
      telemetry = { enable = false },
    },
  },
}

-- Setup neovim lua configuration
require('neodev').setup()

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
  function(server_name)
    require('lspconfig')[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
    }
  end,
}

-- nvim-cmp setup
local cmp = require 'cmp'
local luasnip = require 'luasnip'

luasnip.config.setup {}

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete {},
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  },
  sources = {
    { name = 'nvim_lsp',                max_item_count = 15 },
    { name = 'nvim_lsp_signature_help', max_item_count = 5 },
    { name = 'luasnip',                 max_item_count = 5 },
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  experimental = {
    ghost_text = false,
  },
}

-- [[ Basic Keymaps ]]
require("config.keymaps")

-- [[ Autocmds ]]
require("config.autocmds")
-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
