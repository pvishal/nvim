-- See `:help vim.o`

-- Disable swap file
vim.opt.swapfile = false

-- Set highlight on search
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- Make line numbers default
vim.wo.number = true

-- And let those line numbers be relative
vim.wo.relativenumber = true

-- Enable mouse mode
vim.opt.mouse = 'a'

-- convert tabs to spaces
vim.opt.expandtab = true

-- the number of spaces inserted for each indentation
vim.opt.shiftwidth = 4

-- insert 2 spaces for a tab
vim.opt.tabstop = 4

-- Stop scroll a few characters before
vim.opt.scrolloff = 5

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.opt.clipboard = 'unnamedplus'

-- Enable break indent
vim.opt.breakindent = true

-- Save undo history
vim.opt.undofile = true

-- Case insensitive searching UNLESS /C or capital in search
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = "yes"

-- Only the last window will have the status line
vim.opt.laststatus = 3

-- Decrease update time
vim.opt.updatetime = 250
vim.opt.timeout = true
vim.opt.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.opt.completeopt = 'menuone,noselect'

-- NOTE: You should make sure your terminal supports this
vim.opt.termguicolors = true

-- Use better fillchars for diffs
vim.opt.fillchars:append { diff = " " }

-- force all horizontal splits to go below current window
vim.opt.splitbelow = true
-- force all vertical splits to go to the right of current window
vim.opt.splitright = true

-- Whether to highlight the current line
vim.opt.cursorline = true
